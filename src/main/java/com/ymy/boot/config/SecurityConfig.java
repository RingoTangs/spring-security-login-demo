package com.ymy.boot.config;

import com.ymy.boot.auth.sms.SmsAuthenticationProvider;
import com.ymy.boot.config.handler.SessionExpiredHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.RememberMeServices;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.session.SessionInformationExpiredStrategy;

import javax.annotation.Resource;

import static com.ymy.boot.constant.AuthConstant.*;


/**
 * Spring Security 主配置类
 *
 * @author Ringo
 * @date 2021/5/9 20:11
 */
@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     * {@link UserDetailsService} 实现类
     */
    @Resource
    private UserDetailsService userService;

    /**
     * 注销登录处理器
     */
    @Resource
    private LogoutSuccessHandler logoutSuccessHandler;

    /**
     * 请求失败处理器
     */
    @Resource
    private AuthenticationEntryPoint requestFailureHandler;

    /**
     * 短信验证码 + 手机号登录配置
     */
    @Resource
    private SmsAuthenticationConfig smsAuthenticationConfig;

    /**
     * 图片验证码 + 用户名密码登录配置
     */
    @Resource
    private ImageAuthenticationConfig imageAuthenticationConfig;


    /**
     * remember-me 服务
     */
    @Resource
    private RememberMeServices rememberMeServices;

    @Resource
    private SmsAuthenticationProvider smsAuthenticationProvider;


    @Bean
    public SessionInformationExpiredStrategy sessionInformationExpiredStrategy() {
        return new SessionExpiredHandler();
    }


    /**
     * 配置全局 AuthenticationManager
     * 1、auth.userDetailsService()：默认配置 DaoAuthenticationProvider 并加入到 ProviderManager 中。
     * 2、auth.authenticationProvider()：在 ProviderManager 中添加 AuthenticationProvider。
     */
    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        // 添加自定义的 AuthenticationProvider
        auth.authenticationProvider(smsAuthenticationProvider);
        auth.userDetailsService(userService);
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        // 静态资源放行
        web.ignoring().antMatchers(
                "/css/**", "/js/**", "/index.html", "/img/**", "/webjars/**",
                "/fonts/**", "/favicon.ico", "/JsonLogin.html"
        );
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.csrf().disable()
                .authorizeRequests()
                .antMatchers(SMS_CODE_API, IMAGE_CODE_API, MOBILE_LOGIN_PROCESSOR_URL, USERNAME_LOGIN_PROCESSOR_URL)
                .permitAll()
                .anyRequest()
                .authenticated()

//                .and()
//                .formLogin()

                // 开启 remember-me 功能
                .and()
                .rememberMe()
                .rememberMeServices(rememberMeServices)       // 在RememberMeAuthenticationFilter中加入rememberMeServices

                .and()
                .logout()
                .logoutUrl(LOGOUT_URL)
                .logoutSuccessHandler(logoutSuccessHandler)

                .and()
                .exceptionHandling()
                .authenticationEntryPoint(requestFailureHandler)

                .and()
                // 短信验证码 + 手机号登录加到 Spring Security 中
                .apply(smsAuthenticationConfig)

                .and()
                // 图片验证码 + 用户名密码登录加入到 Spring Security 中
                .apply(imageAuthenticationConfig)

                // session 会话管理, 注意 User 的 Equals() HashCode() 一定要重写
                .and()
                .sessionManagement()
                .maximumSessions(1)
                .expiredSessionStrategy(sessionInformationExpiredStrategy());
    }
}
